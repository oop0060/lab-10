package com.arukkha.week10;

public class Triangle extends Shape{
    private double a;
    private double b;
    private double c;
    private double s;

    public Triangle(double a, double b, double c) {
        super("triangle");
        this.a = a;
        this.b = b;
        this.c = c;
        this.s = a + b + c / 2;

    }
    public double geta() {
        return a;
    }
    public double getb() {
        return b;
    }
    public double getc() {
        return c;
    }
    public double gets() {
        return a + b + c / 2;
    }
    @Override
    public double calArea() {
        return Math.sqrt(this.s * (this.s - this.a) * (this.s - this.b) * (this.s - this.c));
    }
    @Override
    public double calPerimeter() {
        return this.a + this.b + this.c;
    }
    @Override
    public String toString() {
        return this.getName() + this.a + this.a + this.a;
    }
}
